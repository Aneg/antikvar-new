<?php

namespace app\controllers;

use app\models\Category;
use app\models\Good;
use app\modules\admin\models\GoodSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class GoodController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays a single Good model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param int $page
     * @param null $category_id
     * @return string
     */
    public function actionSearch($page = 1, $category_id = null)
    {
        $limit = 9;
        $searchModel = new GoodSearch();
        $count = $searchModel->search(Yii::$app->request->queryParams, $category_id)->query->limit($limit)->offset($limit* ($page - 1))->count();
        $countPage = ceil($count / $limit);
        $models = $searchModel->search(Yii::$app->request->queryParams, $category_id)->query->limit($limit)->offset($limit* ($page - 1))->all();

        $category = $category_id ? Category::findOne($category_id) : null;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'models' => $models,
            'countPage' => $countPage,
            'page' => $page,
            'category' => $category
        ]);
    }

    /**
     * Finds the Good model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Good the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Good::findOneByClient(['good.id' => $id], ['values', 'values.field', 'category', 'image'])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}