<?php


namespace app\rbac;


class Permissions
{
    /** Редактирование данных пользователя */
    const MANAGE_USERS = 'manageUsers';
    /** Создавать и редактировать товары */
    const EDIT_GOOD = 'editGood';
    /** Изменять словари */
    const EDIT_DICTIONATY = 'editDictionary';
    /** Просмотров контактов продавцов */
    const SEE_CONTACT = 'seeContact';
}