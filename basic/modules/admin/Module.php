<?php

namespace app\modules\admin;
use app\models\User;
use yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';
    public $components = 'app\modules\admin\commands';

    public $layout = "main.php";

    public $itemMenu = [
        ['label' => 'Категории', 'url' => ['/admin/category/index']],
        ['label' => 'Словари', 'url' => ['/admin/dictionary/index']],
        ['label' => 'Товары', 'url' => ['/admin/good/index']],
    ];


    public function init()
    {
        parent::init();
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
        \Yii::configure($this, require($path));
        // custom initialization code goes here
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN, User::ROLE_MANAGER],
                    ],
                ],
            ],
        ];
    }
}
