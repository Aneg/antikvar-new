<?php

namespace app\modules\admin\models;

use app\models\Good;
use app\models\GoodValue;
use app\models\Image;
use app\models\Value;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class GoodForm extends Good
{

    private $items = [];
    
    /**
     * @var UploadedFile
     */
    public $file;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'maxSize' => 40000000, /*'extensions'=>'jpg, jpeg, gif, png'*/],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if (!$this->validate()) return false;
        if (!parent::save($runValidation, $attributeNames = null)) return false;

        foreach ($this->items['GoodValue'] as $value) {
            /** @var GoodValue  $value */
            $value->good_id = $this->id;
            if (!$value->save()) {
            }
        }
        if (!empty($this->items['Value'])) {
            foreach ($this->items['Value'] as $value) {
                /** @var Value  $value */
                if ($value->id && $value->save()) {
                } elseif (!$value->id && $value->save() && !$this->link('values', $value)) {
                }
            }
        }

        if ($this->file) {
            $fileName = (string)microtime(true).rand(0,1000). '.' . $this->file->extension;
            $path = '/image/' . $fileName;
            if ($this->file->saveAs(Yii::getAlias('@webroot'.$path))) {
                if ($this->image) {
                    $absPass = Yii::getAlias("@webroot".$this->image->image);
                    if (is_file($absPass)) unlink($absPass);
                    $this->image->name = $fileName;
                    $this->image->image = $path;
                    $this->image->save();
                } else {
                    $image = new Image();
                    $image->name = $fileName;
                    $image->image = $path;
                    $image->save();
                    $this->image_id = $image->id;
                    $this->save();
                }
                return true;
            } else {
                return false;
            }   
        }
        return true;
    }

    public function getIsNewRecord() {
        return empty($this->id);
    }

    public function getItems()
    {
        return $this->items;
    }

    public function load($data, $formName = null) {

        $scope = $formName === null ? $this->formName() : $formName;
        if (isset($data[$scope])) {
            $this->setAttributes($data[$scope], false);
        }
        
        $this->file = UploadedFile::getInstance($this, 'file');

        $this->items = $this->category->getItemsToUpdate(['good_id' => $this->id]);

        foreach ($this->items as $key1 => $value) {
            if (!Model::loadMultiple($this->items[$key1], Yii::$app->request->post(), $key1)) {
                return false;
            }
        }
        return true;
    }
}