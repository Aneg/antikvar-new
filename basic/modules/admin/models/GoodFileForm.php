<?php

namespace app\modules\admin\models;

use app\models\Image;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class GoodFileForm extends Image
{

    public $good_id;
    /**
     * @var UploadedFile
     */
    public $file;
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id'], 'integer'],
            [['file'], 'file', 'maxSize' => 40000000, 'extensions'=>'jpg, gif, png'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->validate()) {
            $image = new Image();
            $image->name = $this->file->baseName;
            $image->image = '/image/' . $this->file->baseName . '.' . $this->file->extension;
            if ($this->file->saveAs('image/' . $this->file->baseName . '.' . $this->file->extension)) {
                if ($image->save()) {
                    $this->id = $image->id;
                    $this->name = $image->name;
                    $this->image = $image->image;
                    return true;
                }
            }
        }
        return false;
    }

    public function getIsNewRecord() {
        return empty($this->good_id);
    }
}