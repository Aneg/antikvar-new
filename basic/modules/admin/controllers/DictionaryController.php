<?php

namespace app\modules\admin\controllers;

use app\models\Category;
use app\models\Field;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class DictionaryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $categories = Category::find()->with('fields')->all();
        return $this->render('index', [
            'categories' => $categories,
        ]);
    }

    public function actionUpdate($id) {
        $model =  Field::find()
            ->andFilterWhere(['id' => $id])
            ->with('values')
            ->one();
        return $this->render('update',[
            'model' => $model,

        ]);
    }

    /**
     * Finds the Field model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Field the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Field::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}