<?php

namespace app\modules\admin\controllers;

use app\models\GoodImage;
use app\models\GoodValue;
use app\models\Image;
use app\models\Label;
use app\models\Value;
use app\modules\admin\models\GoodFileForm;
use app\modules\admin\models\GoodForm;
use Yii;
use app\models\Good;
use app\modules\admin\models\GoodSearch;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * GoodController implements the CRUD actions for Good model.
 */
class GoodController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Lists all Good models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GoodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Good model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GoodForm();
        // TODO: Сделать нормально.
        if (isset($_POST["Good"]["category_id"])) {}
        $model->category_id = $_POST["Good"]["category_id"];

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->save())
            {
                return $this->redirect(Url::to(['update', 'id' => $model->id]));
            }
        }
        return $this->render('create', [
            "model" => $model,
        ]);
    }

    /**
     * Updates an existing Good model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = GoodForm::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()))
            {
                $model->save();
            }
        }
        return $this->render('update', [
            "model" => $model,
        ]);
    }

    /**
     * Updates an existing Good model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionAddFile($id)
    {
        // TODO: запилить мультизагрузку картинок
        // TODO: перепилить $link
        // TODO: запилить сохранение файлов через AJAX
        $model = $this->findModel($id);
        if ($_FILES['GoodFileForm']) {
            $file = new GoodFileForm();
            if ($file->file = UploadedFile::getInstance($file, 'file')) {
                if ($file->save() && $file->id) {
                    $link = new GoodImage();
                    $link->good_id = $model->id;
                    $link->image_id = $file->id;
                    if ($link->save()) {
                    }
                }
            }
        }
        return $this->redirect(Url::to(['update', 'id' => $model->id]));
    }

    /**
     * Deletes an existing Good model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        foreach ($model->images as $image) {
            $model->unlink('images',$image, true);
        }
        foreach ($model->labels as $label) {
            $model->unlink('labels',$label, true);
        }
        if ($model->image) {
            $model->unlink('image',$model->image, true);
            foreach ($model->labels as $label) {
                $model->unlink('labels',$label);
            }
            foreach ($model->values as $value) {
                $model->unlink('values', $value, true);
            }
        }
        // TODO: Удаление связей;
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param Good $model
     * @param array $labelsId
     * @return array
     */
    protected function getIdLabelsForLinkAndUnlink(Good $model,array $labelsId)
    {
        $del = [];
        foreach ($model->labels as $label) {
            $key = array_search("$label->id", $labelsId);
            if ($key !== false) {
                unset($labelsId[$key]);
            } else {
                $del[] = $label->id;
            }
        }
        return [$labelsId, $del];
    }

    /**
     * @param integer $imageId
     * @return array
     */
    public function actionRemoveImage($imageId, $id)
    {
        $image = $this->findImage($imageId);
        $model = $this->findModel($id);

        $ststus = 'error';
        if ($image && $model) {
            if ($model->unlink('images', $image, true) == null && $image->delete()) {
                $ststus = 'success';
            }
        }

        return [
            'status' => $ststus,
        ];
    }

    /**
     * Finds the Good model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Good the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Good::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Good model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Good the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findLabel($id)
    {
        if (($model = Label::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Good model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Image the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findImage($id)
    {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
