<?php

namespace app\modules\admin\controllers;

use app\models\Label;

use Yii;
use app\models\Category;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class LabelController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * Update fields for category
     * @param integer $categoryId
     * @return string
     */
    public function actionSave($categoryId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $category = $this->findCategory($categoryId);
        $post = Yii::$app->request->post();
        $isNew = empty($post['id']);
        if ($isNew) {
            $model = Label::find()->where(['name' => $post['name']])->one();
            if ($model && $category->link('labels',$model) === null) {
                return [
                    'status' => 'success',
                    'model' => ['id' => $model->id],
                ];
            } elseif ($model) {
                return ['status' => 'error'];
            } else {
                $model = new Label();
            }
        }
        if (!($model->load(['Label' => $post]) && $model->save() && (!$isNew || $category->link('labels',$model) === null))) {
            return ['status' => 'error'];
        }
        return [
            'status' => 'success',
            'model' => ['id' => $model->id],
        ];
    }

    /**
     * Deletes an existing Field model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$category_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        /* @var $category \app\models\Category */
        $category = $this->findCategory($category_id);

        if ($category->unlink('labels', $model, true) == null) {
            if (!(empty($model->categoryLabels) && empty($model->goodLabels)) && $model->delete()) {
                return ['status' => 'success'];
            }
        }
        return ['status' => 'error'];
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCategory($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Field model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Label the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Label::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}