<?php

namespace app\modules\admin\controllers;

use app\models\Field;
use app\models\FieldType;
use app\models\Value;
use Yii;
use app\models\Category;
use yii\base\Model;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class FieldController extends Controller
{
    //TODO: Добавить опции для вывода форм.
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Update fields for category
     * @param integer $categoryId
     * @return string
     */
    public function actionSave($categoryId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $category = $this->findCategory($categoryId);
        $post = Yii::$app->request->post();
        $isNew = empty($post['id']);
        $model = $isNew ? new Field() : $this->findModel((int)$post['id']);
        if (!($model->load(['Field' => $post]) && $model->save() && (!$isNew || $category->link('fields',$model) === null))) {
            return ['status' => 'error'];
        }
        return [
            'status' => 'success',
            'model' => ['id' => $model->id],
        ];
    }

    /**
     * Deletes an existing Field model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$category_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        /* @var $category \app\models\Category */
        $category = $this->findCategory($category_id);

        if ($category->unlink('fields', $model, true) == null && Value::deleteAll(['field_id' => $id])) {
            return ['status' => 'success'];
        }
        return ['status' => 'error'];
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCategory($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Field model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Field the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Field::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}