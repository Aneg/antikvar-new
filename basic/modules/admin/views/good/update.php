<?php

use \yii\bootstrap\Tabs;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Good */


$this->title = 'Просмотр: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Просмотр';
?>
<div class="good-update">
    <?= $this->render('_form', [
        "model" => $model,
    ]) ?>

    <?= $this->render('_gallery', [
        "model" => $model,
    ]) ?>
</div>

<?php
$this->registerJsFile('js/app/controllers/admin/good/update.js', ['depends' => ['app\modules\admin\AppAsset',]]);

$this->registerCssFile('/css/gallery/elastislide.css');
$this->registerCssFile('/css/gallery/style.css');
?>