<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileinput\FileInput;
use app\modules\admin\models\GoodFileForm;

/* @var $this yii\web\View */
/* @var $model app\models\Good */
/* @var $form yii\widgets\ActiveForm */

$images = new GoodFileForm();
?>
<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">
			<div class="rg-image-wrapper">
				{{if itemsCount > 1}}
					<div class="rg-image-nav">
						<a href="#" class="rg-image-nav-prev">Previous Image</a>
						<a href="#" class="rg-image-nav-next">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-image"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
		</script>
<div id="gallery-container"></div>

<script type="text/template" id="gallery-template">
        <?php $form = ActiveForm::begin(['options' => [
            'action-form' => \yii\helpers\Url::to(['add-file', 'id' => $model->id]),
            'id' => 'good-images-form',
            'enctype' => 'multipart/form-data',
        ]]); ?>
    <div class="row">
        <br>
        <br>
        <div class="col-md-10">
            <?= $form->field($images, 'file', ['template' => '{input}'])->widget(\dosamigos\fileinput\BootstrapFileInput::className(), [
                'options' => [ 'id' => 'galleryform-file', 'accept' => 'image/*', 'multiple' => false],
                'clientOptions' => [
                    'previewFileType' => 'text',
                    'browseClass' => 'btn btn-success',
                    'uploadClass' => 'hidden',
                    'removeClass' => 'btn btn-danger',
                    'removeLabel' => 'Очистить',
                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>',
                    'browseLabel' => 'Выбрать'
                ]
            ]);?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-success addFile']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="row">
        <?php foreach ($model->images as $key => $image) { ?>
            <div class="col-sm-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Area Chart</h3>

                        <div class="box-tools pull-right remove-image" date-imageId="<?= $image->id ?>" date-id="<?= $model->id ?>">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="display: block;">
                        <div class="chart">
                            <img class="img-responsive" src="<?= $image->image ?>" alt="Photo"/>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        <?php } ?>
    </div><!-- rg-gallery -->
</script>
<?php $this->registerJsFile('js/app/views/admin/good/gallery.js', ['depends' => ['app\modules\admin\AppAsset',]]); ?>
