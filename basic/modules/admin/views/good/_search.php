<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\GoodSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="good-search well bs-component">
    <legend>Поиск</legend>
    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>
        <div class="col-sm-2">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-sm-10">
            <?= $form->field($model, 'category_id')->dropDownList($dict['category_view']) ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
