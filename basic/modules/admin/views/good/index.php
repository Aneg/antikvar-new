<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use \app\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\GoodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;

$dict = [
    'category_view' => Category::getList(['view' => true]),
    'category' => Category::getList(),
];

$model = new \app\models\Good();
?>
<div class="good-index">
    <?php  echo $this->render('_search', ['model' => $searchModel, "dict" => $dict]); ?>

    <div class=" well bs-component">
        <legend>Добавить товар</legend>
        <?php $form = ActiveForm::begin(['action' => 'create']); ?>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'category_id', ['template' => '{input}'])->dropDownList($dict['category_view']) ?>
            </div>
            <div class="col-sm-2">
                <?= Html::submitButton("Добавить", ['class' => 'btn btn-success']) ?>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <h3>Список товаров</h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'format' => 'html',
                'value' => function ($model) {
                    /** @var \app\models\Good $model */
                    $color_circle = $model->active ? "text-green" : "text-red";
                    return '<i class="fa fa-fw fa-circle ' . $color_circle . '"></i>';
                }
            ],
            [
                'attribute' => 'image_id',
                'format' => 'html',
                'value' => function ($model) {
                    /** @var \app\models\Good $model */
                    $html = '
                        <a href="'.$model->image->image.'" class="c-lightbox">
                            <img src="'.$model->image->image.'" class="b-preview-image">
                        </a>';
                    return $html;
                }
            ],
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->name, ['update', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category->name;
                }
            ],
            'updated',
            'created',
        ],
    ]); ?>
</div>
