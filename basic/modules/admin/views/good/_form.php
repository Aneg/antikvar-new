<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileinput\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Good */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('js/app/views/admin/good/form.js', ['depends' => ['app\modules\admin\AppAsset',]]);

$isNewRecord = !$model->id;

$selectedLabels = [];
foreach ($model->labels as $label) {
    $selectedLabels[] = $label->id;
}


echo cakebake\bootstrap\select\BootstrapSelectAsset::register($this, [
    'selector' => '.selectpicker', //The jQuery selector (all select forms by default)
    'menuArrow' => true, //You can also show the tick icon on single select
    'tickIcon' => false, //The bootstrap menu arrow can be added
    'selectpickerOptions' => [ //available bootstrap-select data options @see http://silviomoreto.github.io/bootstrap-select/3/#options
        'size' => 5, //example option @see http://silviomoreto.github.io/bootstrap-select/3/#options
        'width' => '100%', //example option @see http://silviomoreto.github.io/bootstrap-select/3/#options
    ],
]);
?>

<div id="form-container"></div>

<script type="text/template" id="form-template">
    <div class="well bs-component">
        <div class="row success">

            <?php $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                ]
            ]); ?>
            <div class="col-md-6">
                <?= Html::submitButton($isNewRecord ? 'Добавить' : 'Обновить', ['class' => $isNewRecord ? 'btn btn-success btn-mx btn-block' : 'btn btn-primary btn-mx btn-block']) ?>
            </div>
            <?php if (!$isNewRecord) { ?>
                <div class="col-md-6">
                    <?= Html::a('Удалить', \yii\helpers\Url::to(['delete', 'id' => $model->id]), ['class' => 'btn btn-danger btn-mx btn-block']) ?>
                </div>
            <?php } ?>
            <br><br><br>
            <div class="col-md-12">
                <?= Html::dropDownList('labels', $selectedLabels, \app\models\Label::getLabelsArray($model->category_id), [
                    'class' => 'selectpicker',
                    'multiple' => true,
                    'data-live-search' => "true",
                    'data-width' => "100%",
                    'data-none-Selected-Text' => 'Вуберите метки',
                    'options' =>
                        [
                            4 => ['selected' => true]
                        ]
                ]) ?>
            </div>
            <br>
            <br>
            <?= $form->field($model, 'id', ['template' => '{input}'])->hiddenInput() ?>
            <?= $form->field($model, 'image_id', ['template' => '{input}'])->hiddenInput() ?>
            <?= $form->field($model, 'category_id', ['template' => '{input}'])->hiddenInput() ?>

            <div class="col-md-4">
                <?php if (!$model->isNewRecord) { ?>
                    <img src="<?= $model->image->image ?>" style="max-width: 500px">
                <?php } ?>
                <?= $form->field($model, 'file')->fileInput(['multiple' => false, 'accept' => 'image/*']) ?>
            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'active', ['template' => '{input}'])->checkbox(); ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput([
                    'maxlength' => 150,
                    'placeholder' => $model->getAttributeLabel('name'),
                ]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'about')->input('text', [
                    'rows' => 6,
                    'placeholder' => $model->getAttributeLabel('about'),
                ]) ?>
            </div>
            <?php
            $goodValue_count = 0;
            $value_count = 0;
            foreach ($model->category->fields as $key => $field) { ?>
                <div class="col-md-12">
                    <label class="control-label" for="good-name"><?= $field->name ?></label>
                    <?= $field->getFieldView($form, [
                        'placeholder' => true,
                        'key' => $field->dictionary ? $goodValue_count : $value_count,
                        'good_id' => $model->id ? $model->id : false,
                    ]) ?>
                </div>
                <?php
                if ($field->dictionary) {
                    $goodValue_count += 1;
                } else {
                    $value_count += 1;
                }
            } ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</script>
