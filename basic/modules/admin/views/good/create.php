<?php

use yii\helpers\Html;
use app\models\Good;

/* @var $this yii\web\View */
/* @var $model app\models\Good */

$this->title = 'Добавление товара:';
$this->params['breadcrumbs'][] = ['label' => 'Goods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="good-create">
    <?= $this->render('_form',[
        "model" => $model,
    ]) ?>

    <div id="fields-container"></div>

    <script type="text/template" id="main-good-admin-template">
        <div id="default-container"></div>
    </script>
</div>

<?php
$this->registerJsFile('js/app/controllers/admin/good/create.js', ['depends' => ['app\modules\admin\AppAsset',]]);
?>
