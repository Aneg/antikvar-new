<?php

use \yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$items = [
    [
        'label' => 'Поля',
        'content' => $this->render('_field', ['model' => $model]),
        'active' => true
    ],
    [
        'label' => 'Метки',
        'content' => $this->render('_label',[
            'model' => $model,
        ]),
    ]
];

$this->registerJsFile('js/app/models/CategoryField.js', ['depends' => ['app\assets\AppAsset',]]);
$this->registerJsFile('js/app/collections/CategoryFields.js', ['depends' => ['app\assets\AppAsset',]]);
$this->registerJsFile('js/app/models/CategoryLabel.js', ['depends' => ['app\assets\AppAsset',]]);
$this->registerJsFile('js/app/collections/CategoryLabels.js', ['depends' => ['app\assets\AppAsset',]]);
?>
<div id="category-form"></div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
<?= Tabs::widget(['items' => $items]); ?>


<?php
$fieldsToJs = [];
$labelsToJs = [];
$allLabelsToJs = [];

foreach ($model->labels as $label) {
    $labelsToJs[] = "{
        name: '{$label->name}',
        id: '{$label->id}'
    }";
}

foreach ($model->fields as $field) {
    $fieldsToJs[] = "{
        name: '{$field->name}',
        id: '{$field->id}',
        required: '{$field->required}',
        dictionary: '{$field->dictionary}',
        about: '{$field->about}',
        type_id: '{$field->type_id}',
        preview: '{$field->preview}',
    }";
}

$this->registerJs(
    "data_fields = [".join(',',$fieldsToJs)."]; category_id = {$model->id};"
    ."autocomplete_labels = ['".join("','",\app\models\Label::getLabelsWithoutThis($model->id))."'];"
    ."data_labels = [".join(',',$labelsToJs)."];",
    yii\web\View::POS_BEGIN);
?>
<?php $this->registerJsFile('js/app/controllers/admin/category/update.js', ['depends' => ['app\modules\admin\AppAsset',]]); ?>

