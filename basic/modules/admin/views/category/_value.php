<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/** @var $value app\models\Value */

$this->registerJsFile('@web/js/main.js');

?>

<?php $form = ActiveForm::begin([
    'action' => $value->isNewRecord ?Url::toRoute(['/admin/value/save']) : Url::toRoute(['/admin/value/update', 'categoryId' => $value->id]),
]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($value, 'value') ?>
        </div>
        <?php if (!$value->isNewRecord)  {?>
            <div class="col-md-1">
                <?= Html::a("<span class='glyphicon glyphicon-save'></span>",['class' => 'btn btn-primary']) ?>
            </div>
        <?php } else { ?>
            <div class="col-md-1">
                <?= Html::a("<span class='glyphicon glyphicon-floppy-disk'></span>",[
                    'class' => 'btn btn-primary',
                    'onclick' => 'addValue()',
                ]) ?>
            </div>
        <?php } ?>
    </div>
<?php ActiveForm::end(); ?>