<?php

use \app\models\Label;
use yii\widgets\ActiveForm;

$label = new Label();
?>



<div id="category-labels"></div>

<script type="text/template" id="category-labels-template">
    <div class="col-md-12">
        <h4>Введите имя новой метки или выберете из уже существующих в выподающем списке.</h4>
    </div>
    <?php $form = ActiveForm::begin([
        'action' => 'add'
    ]); ?>
    <div class="col-md-10">
        <?= $form->field($label, 'name', ['template' => "{input}"])->textInput(['placeholder' => 'Введите название метки',]) ?>
    </div>
    <div class="col-md-2">
        <a class="btn btn-sm btn-success" href="javascript:void(0)" id="save-label"><span class="glyphicon glyphicon-floppy-disk"></span></a>
    </div>
    <?php ActiveForm::end(); ?>
    <div id="labels-table"></div>
</script>

<script type="text/template" id="label-form-template">
    <?php $form = ActiveForm::begin([
        'action' => 'add'
    ]); ?>
    <input type="hidden" id="label-id" class="form-control" name="Label[id]" value="<%= id %>">
    <div style="margin-bottom: 10px; margin-top: 10px;">
        <div class="col-md-9">
            <div class="form-group field-label-name required has-success">
                <input type="text" id="label-name" class="form-control" name="Label[name]" value="<%= name %>" placeholder="Введите название метки">
            </div>
        </div>
        <div class="col-md-3">
            <a class="btn btn-sm btn-success" href="javascript:void(0)" id="save-label"><span class="glyphicon glyphicon-floppy-disk"></span></a>
            <a class="btn btn-sm btn-danger" href="javascript:void(0)" id="drop-label"><span class="glyphicon glyphicon-trash"></span></a>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</script>

<?php $this->registerJsFile('js/app/views/admin/category/labels.js', ['depends' => ['app\modules\admin\AppAsset',]]); ?>