<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('js/app/views/admin/category/form.js', ['depends' => ['app\modules\admin\AppAsset',]]);
?>
<script type="text/template" id="category-form-template">
    <div class="category-form well bs-component">
        <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить категорию' : 'Обновить категорию', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?php if (!$model->isNewRecord) { ?>
                <?= Html::a('Удалить категорию', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => "Удалить категорию {$model->name}?",
                        'method' => 'post',
                    ],
                ]) ?>
            <?php } ?>
        </div>
        <div class="row">
<!--            <div class="col-md-4">
                <?= $form->field($model, 'parent_id')->dropDownList(Category::getParentList(),['prompt'=>'']) ?>
            </div>
-->
            <?php if (!$model->isNewRecord) { ?>
                <div class="col-md-3">
                    <?= $form->field($model, 'updated')->textInput(['readonly' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'created')->textInput(['readonly' => true]) ?>
                </div>
            <?php } ?>
            <div class="col-md-10">
                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
            </div>
            <?php if (!$model->isNewRecord) { ?>
                <div class="col-md-2">
                    <label class="control-label" for="category-active"></label>
                    <?= $form->field($model, 'active')->checkbox() ?>
                </div>
            <?php } ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</script>