<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CategoryTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'danger',
        ],
        'columns' => [
            'id',
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->name, Url::toRoute(['update', 'id' => $model->id]));
                }
            ],
            [
                'attribute' => 'active',
                'format' => 'html',
                'value' => function ($model) {
                    return !empty($model->active) ? '<span class="glyphicon glyphicon-ok"></span>': '<span class="glyphicon glyphicon-remove"></span>';
                }
            ],
            [
                'attribute' => 'updated',
                'format' => 'html',
                'options' => [
                    'class' => 'col-md-2',
                ],
                'value' => function ($model) {
                    return $model->updated;
                }
            ],
            [
                'attribute' => 'created',
                'format' => 'html',
                'options' => [
                    'class' => 'col-md-2',
                ],
                'value' => function ($model) {
                    return $model->created;
                }
            ],
        ],
    ]); ?>

</div>
