<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\CategoryTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-search well bs-component">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'id') ?>
            </div>
<!--            <div class="col-sm-5">
                <?= $form->field($model, 'parent_id')->dropDownList(\app\models\Category::getParentList(),['prompt'=>'']) ?>
            </div>
-->

            <div class="col-sm-5">
                <?= $form->field($model, 'name') ?>
            </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сбросить фильтр', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
