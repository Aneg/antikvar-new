<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use \app\models\FieldType;
use \app\models\Field;;

/* @var $model app\models\Category */
/* @var $this yii\web\View */

$fieldTemplate = new Field();
//
//foreach ($model->labels as $label) {
//    $labelsToJs[] = "{
//        name: '{$label->name}',
//        id: '{$label->id}'
//    }";
//}
//
//foreach ($model->fields as $field) {
//    $fieldsToJs[] = "{
//        name: '{$field->name}',
//        id: '{$field->id}',
//        required: '{$field->required}',
//        dictionary: '{$field->dictionary}',
//        about: '{$field->about}',
//        type_id: '{$field->type_id}',
//    }";
//}
//
//$this->registerJs("data_fields = [".join(',',$fieldsToJs)."]; category_id = {$model->id};"."data_labels = [".join(',',$labelsToJs)."];",yii\web\View::POS_BEGIN);
?>
<div id="category-fields"></div>

<script type="text/template" id="category-fields-template">
    <h4><strong>Дополнительные поля</strong></h4>
    <?php $form = ActiveForm::begin([
        'action' => Url::toRoute(['/admin/field/update', 'categoryId' => $model->id]),
        'id' => 'fieldsUpdateForm'
    ]); ?>
        <?= Html::a('Добавить поле', 'javascript:void(0)', [
            'class' => 'btn btn-success',
            "id" => 'add-field',
        ]) ?>

        <table class="table">
            <tr>
                <td style="width: 485px">
                    <strong>Название</strong>
                </td>
                <td style="width: 100px">
                    <strong>Тип</strong>
                </td>
                <td style="width: 75px">
                    <strong>Словарь</strong>
                </td>
                <td style="width: 75px">
                    <strong>Обязательно</strong>
                </td>
                <td style="width: 75px">
                    <strong>Превью</strong>
                </td>
            </tr>
        </table>
        <div id="fields-table"></div>
    <?php ActiveForm::end(); ?>
</script>

<script type="text/template" id="field-form-template">
    <?php $form = ActiveForm::begin([
        'action' => 'save-field',
    ]); ?>
    <input type="hidden" id="field-<%= id %>-id" class="form-control" name="Field[<%= id %>][id]" value="<%= id %>">
        <tr>
            <td class="" style="width: 430px">
                <div class="form-group field-field-<%= id %>-name">
                    <input type="text" id="field-<%= id %>-name" class="form-control" name="Field[<%= id %>][name]" value="<%= name %>" placeholder="Введите название поля">
                </div>
            </td>
            <td style="width: 100px">
                <div class="form-group field-field-<%= id %>-type_id required">
                    <select id="field-<%= id %>-type_id" class="form-control" name="Field[<%= id %>][type_id]">
                        <?php foreach (FieldType::getList() as $id => $type) { ?>
                            <option  <% if (type_id == <?= $id ?>) { %>selected <% } %> value="<?= $id ?>"><?= $type ?></option>
                        <?php } ?>
                    </select>
                </div>
            </td>
            <td style="width: 75px">
                <div class="form-group field-field-<%= id %>-dictionary">
                    <select id="field-<%= id %>-dictionary" class="form-control" name="Field[<%= id %>][dictionary]">
                        <option value="1" <% if (dictionary == 1) { %>selected <% } %>>Да</option>
                        <option value="0" <% if (dictionary == 0) { %>selected <% } %>>Нет</option>
                    </select>
                </div>
            </td>
            <td style="width: 75px">
                <div class="form-group field-field-<%= id %>-required">
                    <select id="field-<%= id %>-required" class="form-control" name="Field[<%= id %>][required]">
                        <option <% if (required == 1) { %>selected <% } %> value="1">Да</option>
                        <option <% if (required == 0) { %>selected <% } %> value="0">Нет</option>
                    </select>
                </div>
            </td>
            <td style="width: 75px">
                <div class="form-group field-field-<%= id %>-preview">
                    <select id="field-<%= id %>-preview" class="form-control" name="Field[<%= id %>][preview]">
                        <option <% if (preview == 1) { %>selected <% } %> value="1">Да</option>
                        <option <% if (preview == 0) { %>selected <% } %> value="0">Нет</option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div class="form-group field-field-<%= id %>-about">
                    <input type="text" id="field-<%= id %>-about" class="form-control" name="Field[<%= id %>][about]" value="<%= about %>" placeholder="Введите пояснение к полю">
                </div>
            </td>
            <td>
                <a class="btn btn-sm btn-success" href="javascript:void(0)" id="save-field"><span class="glyphicon glyphicon-floppy-disk"></span></a>
                <a class="btn btn-sm btn-danger" href="javascript:void(0)" id="drop-field"><span class="glyphicon glyphicon-trash"></span></a>
                <!--
                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', 'javascript:void(0)', [
                    'class' => 'btn btn-sm btn-danger',
                    'data' => [
                        'confirm' => "Удалить поле?",
                        'method' => 'post',
                    ],
                ]) ?>
                -->
            </td>
        </tr>
    <?php ActiveForm::end(); ?>
</script>

<?php $this->registerJsFile('js/app/views/admin/category/fields.js', ['depends' => ['app\modules\admin\AppAsset',]]); ?>
