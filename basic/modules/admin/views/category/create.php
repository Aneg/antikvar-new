<?php

use yii\helpers\Html;
use \yii\bootstrap\Tabs;


/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = 'Добавление категории';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="category-form"></div>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

<?php $this->registerJsFile('js/app/controllers/admin/category/create.js', ['depends' => ['app\modules\admin\AppAsset',]]); ?>