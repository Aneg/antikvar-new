<?php

use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $categories array */

$this->title = 'Словари';
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">

            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-group" id="accordion">
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <?php
                    /** @var \app\models\Category $category */
                    foreach ($categories as $category) { ?>
                    <div class="panel box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $category->id ?>">
                                    <?= $category->name ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse<?= $category->id ?>" class="panel-collapse collapse">
                            <div class="box-body">
                                <ul>
                                    <?php
                                    /** @var \app\models\Field $field */
                                    foreach ($category->fields as $field) {
                                        if ($field->dictionary) {
                                            $url = Url::toRoute(['update','id' => $field->id]);
                                    ?>
                                        <li>
                                            <?= "<a href='{$url}'>{$field->name}</a> - $field->about" ?>
                                        </li>

                                    <?php
                                        }
                                    } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
