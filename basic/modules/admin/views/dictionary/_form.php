<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use \app\models\FieldType;
use \app\models\Value;

/* @var $this yii\web\View */
/* @var $model app\models\Field */


$modelValue = new Value();
$modelValue->field_id = $model->id;
?>

<?php $form = ActiveForm::begin([
//    'action' => Url::toRoute(['/admin/field/update', 'categoryId' => $model->id]),
    'id' => 'fieldForm'
]); ?>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, "name", [
            'template' => "{input}"
        ])->textInput(['placeholder' => 'Введите название поля']) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, "type_id",[
            'template' => "{input}"
        ])->dropDownList(FieldType::getList()) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'updated', ['template' => '{input}'])->textInput(['readonly' => true]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

