<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Field */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Словари', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// TODO: Сделать проверку типов при добавлении и блок изменения типа
$this->registerJsFile('/js/load.js');

$value = new \app\models\Value();
$value->field_id = $model->id;
?>

<?php
Modal::begin([
    'options' => [
        'id' => 'value-modal-init',
    ],
    'header' => "<h3>Добавление значения в словарь \"{$model->name}\"</h3>",
    'size' => 'modal-lg',
]);

// TODO: Сделать изменение кнопки в зависимости от действия
$form = ActiveForm::begin([
    'id' => 'dictionary-item-form',
    'action' => Url::to(['/admin/value/create'])
]);
?>
<div class="row" id="value-content-form">

    <?php $form = ActiveForm::begin([
        'id' => 'dictionary-item-form',
        'action' => Url::to(['/admin/value/create'])
    ]); ?>

        <?= $form->field($value, "field_id",[
            'template' => "{input}"
        ])->hiddenInput() ?>
        <?= $form->field($value, "id",[
            'template' => "{input}"
        ])->hiddenInput(['id' => 'input-id']) ?>
        <div class="col-md-9">
            <?= $form->field($value, "value", [
                'template' => "{input}"
            ])->textInput([
                'placeholder' => 'Введите значение',
                'maxlength' => 150,
                'id' => 'input-value',
            ]) ?>
        </div>
        <div class="form-group col-md-1">
            <?= Html::a('Добавить', 'javascript:void(0)', [
                'id' => 'submit-button-dictionary',
                'class' => 'btn btn-success',
                'onclick' => "load.submitFormDictionary({$model->id})"
            ]) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
ActiveForm::end();
Modal::end();
?>

<div class="dictionary-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

<?= Html::a('Добавить в словарь', ['#'], [
    'data-toggle' => 'modal',
    'data-target' => '#value-modal-init',
    'class' => 'btn btn-success',
    'onclick' => "load.createValue({$model->id})"
]) ?>

<div id="dictionary-search-value">

</div>

<?php
$this->registerJs("load.init({$model->id}), 'dictionary-form'");
?>
