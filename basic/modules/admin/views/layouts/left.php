<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Поиск..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Товары',
                        'icon' => 'fa fa-plus-square',
                        'url' => ['/admin/good/index'],
                        'visible' => \Yii::$app->user->can(\app\rbac\Permissions::EDIT_GOOD)],
                    [
                        'label' => 'Структура данных',
                        'icon' => 'fa fa-file-code-o',
                        'url' => '#',
                        'visible' => \Yii::$app->user->can(\app\rbac\Permissions::EDIT_DICTIONATY),
                        'items' => [
                            ['label' => 'Категории', 'icon' => 'fa fa-circle-o', 'url' => ['/admin/category/index']],
                            ['label' => 'Словари', 'icon' => 'fa fa-circle-o', 'url' => ['/admin/dictionary/index']],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
