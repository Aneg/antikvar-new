<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Value */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="value-form">

    <?php $form = ActiveForm::begin([
        'id' => 'dictionary-item-form',
//        'action' => Url::to(['/admin/value/create'])
    ]); ?>

    <div class="row">
        <div class="col-md-9">
            <?= $form->field($value, "value", [
                'template' => "{input}"
            ])->textInput([
                'placeholder' => 'Введите значение',
                'maxlength' => 150
            ]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($value, "field_id",[
                'template' => "{input}"
            ])->hiddenInput() ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::a('Добавить', 'javascript:void(0)', [
            'id' => 'submit-button-dictionary',
            'class' => 'btn btn-success',
            'onclick' => "load.submitFormDictionary({$model->id})"
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
