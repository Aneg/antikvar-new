<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $field_id integer */

$this->title = 'Values';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="value-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'col-md-1',
                ],
            ],
            'value',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => "{update} {delete}",
                'options' => [
                    'style' => 'width: 110px',
                ],
                'buttons' =>[
                    'update' => function($url, $model) {
                        // todo: Переделать кнопки
                        return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', '#', [
                            'title' => 'Обновить',
                            'data-pjax' => '0',
                            'data-toggle' => 'modal',
                            'data-target' => '#value-modal-init',
                            'class' => 'grid-update btn btn-warning',
                            'onclick' => "load.updateValue({$model->id})",
                        ]);
                    },
                    'delete' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => 'Удалить',
                            'data-pjax' => '0',
                            'class' => 'grid-update btn btn-danger',
                            'onclick' => $this->registerJs("load.deleteValue({$model->id})"),

                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
