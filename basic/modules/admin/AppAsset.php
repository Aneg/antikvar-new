<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin;

use yii\web\AssetBundle;

/**
 * TODO: установить боуер и гальп.
 * TODO: разделить бэк и фронт.
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admin/css/site.css',
    ];
    public $js = [
        'js/lib/underscore.js',
        'js/lib/backbone.js',
        'js/lib/backbone.marionette.js',
        'js/lib/jquery-ui.js',
        'admin/js/main.js',
      ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_END];
}
