<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

$this->title = 'Главная';

limion\bootstraplightbox\BootstrapMediaLightboxAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="main-body-site ">

<?php $this->beginBody() ?>

    <div class="wrap">
        <div class="nav-border">
            <?php
            NavBar::begin([
                'brandLabel' => 'Антиквариат',
                'brandUrl' => '/',
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => [
                    ['label' => 'Каталог', 'url' => ['/good/search']],
                    ['label' => 'Контакты', 'url' => ['/site/contact']],
                    ['label' => 'О нас', 'url' => ['/site/about']],
                ],
            ]);

            $userMenu = [];
            if (Yii::$app->user->isGuest) {
                $userMenu = [
                    ['label' => 'Войти', 'url' => ['/site/login']],
                    ['label' => 'Зарегистрироваться', 'url' => ['/site/reg']],
                ];
            } else {
                $userMenu = [
                    ['label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']],
                ];
            }

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $userMenu,
            ]);
            NavBar::end();
            ?>
        </div>
        <div class="container" >
            <div class="row row-centered" style="border: 2px solid #333333">
                <div class="col-md-2 category-bar" style="border: 2px solid #333333">
                    <ul class="nav nav-pills nav-stacked" style="max-width: 260px;">
                        <legend>Категории</legend>
                        <?php foreach (\app\models\Category::find()
                                           ->select(['category.id as id', 'category.name as name'])
                                           ->joinWith(['goods'])
                                           ->groupBy(['category.id'])->all() as $row) {

                            ?>
                            <li class="">
                                <a href="<?= \yii\helpers\Url::toRoute(["good/search", 'page' => 1, "category_id" => $row['id']]) ?>">
                                    <span class="badge pull-right"><?= '' ?></span>
                                    <?= $row['name'] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-md-9 content-container" style="border: 2px solid #333333">
                    <?php Breadcrumbs::widget([
                        'homeLink' => [
                            'label' => Yii::t('yii', 'Админка'),
                            'url' => '/admin',
                        ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= $content ?>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>

    <div class="container">
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
