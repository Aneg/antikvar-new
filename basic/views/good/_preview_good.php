<?php

/* @var $good \app\models\Good */

?>
<div class="col-sm-6 col-md-4 col-fixed ">
    <div class="thumbnail">
        <a href="<?= \yii\helpers\Url::to(['/good/view', 'id' => $good->id]) ?>">
            <h4><?= $good->name ?></h4>
            <img class="preview-image-good" src="<?= $good->image->image ?>" alt="<?= $good->name ?>" data-src="holder.js/300x200">
        </a>
        <div class="caption">
            <p>
                <label>Категория: </label>
                <a href="<?= \yii\helpers\Url::toRoute(["good/search", 'page' => 1, "category_id" => $good->category_id]) ?>"><?= $good->category->name ?></a>
            </p>
            <?php foreach ($good->values as $value) { ?>
                <?php if ($value->field->preview) { ?>
                    <p>
                        <label><?= $value->field->name ?></label>: <?= $value->value ?>
                    </p>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>