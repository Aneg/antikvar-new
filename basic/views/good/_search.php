<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\GoodSearch */
/* @var $category \app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="good-search well bs-component">
    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => ['search', 'page' => 1, 'category_id' => $category ? $category->id : null],
            'method' => 'get',
        ]); ?>
        <div class="col-sm-2">
            <legend>Поиск:</legend>
        </div>
        <div class="col-sm-8">
            <?= $form->field($model, 'name', ['template' => '{input}']) ?>
        </div>

        <div class="col-sm-2 form-group">
            <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
