<?php
/* @var $model \app\models\Good */
?>

<div class="view-div-good">
    <h4><?= $model->name ?></h4>
    <div class="row">
        <div class="col-md-12">
            <a style="float:left; margin: 7px 7px 7px 0;" href="<?= $model->image->image ?>" class="c-lightbox">
                <img src="<?= $model->image->image ?>" alt="<?= $model->name ?>" class="img-thumbnail preview-image-good">
            </a>
            <p>
                <label>Категория: </label>
                <a href="#"><?= $model->category->name ?></a>
            </p>
            <?php foreach ($model->values as $value) { ?>
                <p>
                    <label><?= $value->field->name ?></label>: <?= $value->value ?>
                </p>
            <?php } ?>
            <p><?= $model->about ?></p>
        </div>
    </div>
    <div class="row">
        <?php foreach ($model->images as $key => $image) { ?>
        <div class="col-sm-6 col-md-4 col-fixed-image">
            <a data-toggle="lightbox" href="<?= $image->image ?>" class="c-lightbox img-thumbnail">
                <div class="thumbnail">
                    <img class="preview-image-good"  src="<?= $image->image ?>" >
                </div>
            </a>
        </div>

        <?php } ?>
    </div>
</div>

