<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use \app\models\Category;

/* @var $this yii\web\View */
/* @var $models array */
/* @var $category Category */
/* @var $countPage integer */
/* @var $searchModel app\modules\admin\models\GoodSearch */


$this->title = $category ? $category->name : "Товары";
$this->params['breadcrumbs'][] = $this->title;

$dict = [
    'category_view' => Category::getList(['view' => true]),
    'category' => Category::getList(),
];

?>
<div class="good-index">
    <?php if ($category) { ?>
        <h2>Категория: "<?= $category->name ?>"</h2>
    <?php } ?>

    <?php  echo $this->render('_search', ['model' => $searchModel, "dict" => $dict, 'category' => $category]); ?>

    <div class="site-index">
        <div class="row">
            <?php if (!empty($models)) {
                foreach ($models as $good) { ?>
                    <?= $this->render('../good/_preview_good', ["good" => $good]) ?>
                <?php }
            } else { ?>
                <div class="col-sm-12">
                    <h4>К сожалению по вашему запросу, совпадений не обнаружено.</h4>
                </div>
            <?php } ?>
        </div>
    </div>
    <nav>
        <ul class="pagination">
            <?php if (!($page == 1)) { ?>
                <li>
                    <a href="/good/search?page=<?= $page-1  ?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            <?php } ?>
            <?php for ($i = 1; $i <= $countPage; $i++) { ?>
                <?php if ($i == $page) { ?>
                    <li class="active"><a href="/good/search?page=<?= $i ?>"><?= $i ?></a></li>
                <?php } else { ?>
                    <li><a href="/good/search?page=<?= $i ?>"><?= $i ?></a></li>
                <?php } ?>
            <?php } ?>
            <?php if (!($page == $countPage)) { ?>
                <li>
                    <a href="/good/search?page=<?= $page+1 ?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </nav>
</div>
