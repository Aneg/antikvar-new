<?php


namespace app\commands;

use app\models\User;
use yii\base\InvalidParamException;
use yii\console\Controller;

class RbacController extends Controller
{
    /**
     * Assign role to user
     *
     * @param string $role
     * @param string $username
     *
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     */
    public function actionAssign($role, $username)
    {
        /** @var User $user */
        $user = User::find()
            ->where(['username' => $username])
            ->one();

        if (!$user) {
            throw new InvalidParamException("There is no user \"$username\".");
        }

        $auth = \Yii::$app->authManager;
        $roleObject = $auth->getRole($role);

        if (!$roleObject) {
            throw new InvalidParamException("There is no role \"$role\".");
        }

        $auth->assign($roleObject, $user->id);
    }

    /**
     * Revoke role from user
     *
     * @param string $role
     * @param string $username
     *
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     */
    public function actionRevoke($role, $username)
    {
        /** @var User $user */
        $user = User::find()
            ->where(['username' => $username])
            ->one();

        if (!$user) {
            throw new InvalidParamException("There is no user \"$username\".");
        }

        $auth = \Yii::$app->authManager;
        $roleObject = $auth->getRole($role);

        if (!$roleObject) {
            throw new InvalidParamException("There is no role \"$role\".");
        }

        $auth->revoke($roleObject, $user->id);
    }
}