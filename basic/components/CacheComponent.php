<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\caching\FileCache;

class CacheComponent extends Component
{
    private $cache;
    private $time = 1;
    private $id;

    function __construct($parems = [])
    {
        if (empty($parems['id'])) {
            Yii::error('CacheComponent: ID: is empty');
        }
        if (!empty($parems['time'])) {
            $this->time = $parems['time'];
        }
        $this->cache = new FileCache();
        $this->id = $this->cache->buildKey($parems['id']);
    }

    public function get($fun)
    {
         $res = $this->cache->get($this->id);

        if ($res == false) {
            $res = $fun();
            $this->cache->set(
                $this->id,
                $res,
                $this->time
            );
        }
        return $res;
    }

    public function set($data, $fun = false) {
        $this->cache->set(
            $this->id,
            $data,
            $this->time
        );
        return $data;
    }

}