var Views = Views || {};

Views.field = Marionette.ItemView.extend({
    tagName: "table",
    className: "table well bs-component",
    template: "#field-form-template",

    events: {
        "click #drop-field" : "dropField",
        "click #save-field" : "saveField",
        "change input" : "changeForm",
        "change select" : "changeForm"
    },

    changeForm: function(ev) {
        input = $(ev.target);
        nameClass = 'field-'+this.model.get('id')+'-';
        if (input.attr('id') == nameClass+'name') {
            this.model.set('name', input.val())
        }
        if (input.attr('id') == nameClass+'required') {
            this.model.set('required', input.val())
        }
        if (input.attr('id') == nameClass+'dictionary') {
            this.model.set('dictionary', input.val())
        }
        if (input.attr('id') == nameClass+'about') {
            this.model.set('about', input.val())
        }
        if (input.attr('id') == nameClass+'type_id') {
            this.model.set('type_id', input.val())
        }
        if (input.attr('id') == nameClass+'preview') {
            this.model.set('preview', input.val())
        }
    },

    dropField: function(ev) {
        var field_id = this.model.get('id');
        var model = this;
        if (field_id != 0) {
            $.ajax({
                url: '/admin/field/delete?id='+field_id+'&category_id='+window.category_id,
                type: "POST",
                dataType: 'json',
                success: function(res) {
                    model.destroy();
                },
                error: function(res) {
                    if (res.status == 'error') {
                        alert('Ошибка при сохранении данных.');
                    } else {
                        alert('Ошибка.');

                    }
                }
            });
        } else {
            this.destroy();

        }
    },

    saveField: function(ev) {
        data = JSON.stringify(this.model);
        $.ajax({
            url: '/admin/field/save?categoryId='+window.category_id,
            type: "POST",
            dataType: 'json',
            data: this.model.toJSON(),
            success: function(res) {
                if (res.status == 'success') {
                    $('#field_id').val(res.model.id);
                }
            },
            error: function(res) {
                if (res.status == 'error') {
                    alert('Ошибка при сохранении данных.');
                } else {
                    alert('Ошибка.');

                }
            }
        });
    }
});

Views.fields = Marionette.CompositeView.extend({
    tagName: "div",
    childView: Views.field,
    childViewContainer: "#fields-table",
    template: "#category-fields-template",

    events: {
        "click #add-field" : "addField"
    },

    addField: function(ev) {
        this.collection.add({});
    }
});