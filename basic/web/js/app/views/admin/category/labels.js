var Views = Views || {};

Views.label = Marionette.ItemView.extend({
    tagName: "div",
    className: "col-md-6 well bs-component",
    template: "#label-form-template",

    events: {
        "click #drop-label" : "dropField",
        "click #save-label" : "saveField",
        "change input" : "changeForm"
    },

    changeForm: function(ev) {
        input = $(ev.target);
        if (input.attr('id') == 'label-name') {
            this.model.set('name', input.val())
        }
    },

    dropField: function(ev) {
        var label_id = this.model.get('id');
        var model = this;
        if (label_id != null) {
            $.ajax({
                url: '/admin/label/delete?id='+label_id+'&category_id='+window.category_id,
                type: "POST",
                dataType: 'json',
                success: function(res) {
                    model.destroy();
                },
                error: function(res) {
                    if (res.status == 'error') {
                        alert('Ошибка при сохранении данных.');
                    } else {
                        alert('Ошибка.');

                    }
                }
            });
        } else {
            this.destroy();
        }
    },

    saveField: function(ev) {
        model = this.model;
        $.ajax({
            url: '/admin/label/save?categoryId='+window.category_id,
            type: "POST",
            dataType: 'json',
            data: this.model.toJSON(),
            success: function(res) {
                if (res.status == 'success') {
                    model.set('id', res.model.id);
                    $('#label_id').val(res.model.id);
                }
            },
            error: function(res) {
                if (res.status == 'error') {
                    alert('Ошибка при сохранении данных.');
                } else {
                    alert('Ошибка.');

                }
            }
        });
    }
});

Views.labels = Marionette.CompositeView.extend({
    tagName: "div",
    className: "row",
    childView: Views.label,
    childViewContainer: "#labels-table",
    template: "#category-labels-template",

    events: {
        "click #save-label" : "addField"
    },

    addField: function(ev) {

        var model = new Models.CategoryLabel({name: $('#label-name').val()});
        var collection = this.collection;
        $.ajax({
            url: '/admin/label/save?categoryId='+window.category_id,
            type: "POST",
            dataType: 'json',
            data: model.toJSON(),
            success: function(res) {
                if (res.status == 'success') {
                    model.set('id', res.model.id);
                    collection.add(model);
                }
            },
            error: function(res) {
                if (res.status == 'error') {
                    alert('Ошибка при сохранении данных.');
                } else {
                    alert('Ошибка.');

                }
            }
        });
    },

    onShow: function () {
        input = $(this.el).find("#label-name");
        input.autocomplete({
            minLength: 2,
            source: autocomplete_labels,
            select: function( event, ui ) {
                input.val( ui.item.label); //ui.item is your object from the array
                return false;
            }
        });
    }

});

