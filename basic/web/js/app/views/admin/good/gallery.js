var Views = Views || {};

Views.gallery = Marionette.ItemView.extend({
    template: "#gallery-template",

    events: {
        "click .addFile" : "saveFile",
        "click .remove-image" : "removeImage"
    },

    removeImage: function(ev) {
        imageId = $(ev.target).closest(".remove-image").attr('date-imageId');
        id = $(ev.target).closest(".remove-image").attr('date-id');
        $.ajax({
            url: "/admin/good/remove-image?imageId=" + imageId + "&" + "id=" + id,
            type: 'POST',
            //data: data,
            cache: false,
            dataType: 'json',
            contentType: false, // важно - убираем форматирование данных по умолчанию
            processData: false, // важно - убираем преобразование строк по умолчанию
            success: function(res) {
                console.log("asdasd");
            },
            error: function(res) {
                console.log("error");
            }
        });
    },

    saveFile: function(ev) {
        form = $(ev.target.closest('form'));
        actionForm = form.attr('action-form');
        form.attr('action', actionForm);

        //var data = new FormData();
        //$.each( form.find("#goodfileform-file")[0].files, function( key, value ){
        //    data.append( value.name, value );
        //});
        //
        //$.ajax({
        //    url: actionForm,
        //    type: 'POST',
        //    data: data,
        //    cache: false,
        //    dataType: 'json',
        //    contentType: false, // важно - убираем форматирование данных по умолчанию
        //    processData: false, // важно - убираем преобразование строк по умолчанию
        //    success: function(res) {
        //        console.log("asdasd");
        //    },
        //    error: function(res) {
        //        console.log("error");
        //    }
        //});
    }
});
