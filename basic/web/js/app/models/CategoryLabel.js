var Models = Models || {};

Models.CategoryLabel = Backbone.Model.extend({
    defaults: {
        id: 0,
        name: ''
    }
});