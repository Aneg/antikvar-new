var Models = Models || {};

Models.CategoryField = Backbone.Model.extend({
    defaults: {
        id: 0,
        name: '',
        required: 0,
        dictionary: 0,
        about: '',
        preview: 1,
        type_id: 2
    }
});