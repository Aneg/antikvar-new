var Application = new  Backbone.Marionette.Application();

Application.addRegions({
    main: '#category-form',
    category_fields: '#category-fields',
    category_labels: '#category-labels'
});

Application.mainView = new Views.form();
Application.main.show(Application.mainView);

fields = new Collections.CategoryFields(data_fields);
labals = new Collections.CategoryLabels(data_labels);

Application.adminCategoryFields = new Views.fields({collection: fields});
Application.category_fields.show(Application.adminCategoryFields);

Application.adminCategoryLabels = new Views.labels({collection: labals});
Application.category_labels.show(Application.adminCategoryLabels);


