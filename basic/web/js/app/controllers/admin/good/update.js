var Application = new  Backbone.Marionette.Application();

Application.addRegions({
    form: '#form-container',
    gallery: '#gallery-container'
});

Application.formView = new Views.form();
Application.form.show(Application.formView);

Application.galleryView = new Views.gallery();
Application.gallery.show(Application.galleryView);
