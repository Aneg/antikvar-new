var Application = new  Backbone.Marionette.Application();


Application.addRegions({
    form: '#form-container'
});

Application.formView = new Views.form();
Application.form.show(Application.formView);

