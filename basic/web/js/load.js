(function(){
    function Loader()
    {
    }

    Loader.prototype.init = function(field_id)
    {
        $('#dictionary-search-value').load("/admin/value/index/?field_id="+field_id);
    };

    Loader.prototype.updateValue = function(id)
    {
        $form = $('#dictionary-item-form');
        $form.attr('action', '/admin/value/update/?id='+id);

        $.ajax({
            url: '/admin/value/load/?id='+id,
            type: "GET",
            dataType: 'json',
            success: function(res) {
                $form.find('#input-value').val(res.value);
                $form.find('#input-id').val(res.id);
            }
        });
    };

    Loader.prototype.createValue = function(id)
    {
        $form = $('#dictionary-item-form');
        $form.attr('action', '/admin/value/update');
        $form.find('#input-value').val('');
        $form.find('#input-id').val('');
    };

    Loader.prototype.submitFormDictionary = function(field_id, id)
    {
        form = $('#dictionary-item-form');
        $.ajax({
            url: form.attr('action'),
            type: "POST",
            dataType: 'json',
            data: form.serialize(),
            success: function(res) {
                $('#dictionary-search-value').load("/admin/value/index/?field_id="+field_id);
            }
        });
    };

    window.load = new  Loader();
})();