<?php

namespace app\models;

use app\components\CacheComponent;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "field_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Field[] $fields
 */
class FieldType extends \yii\db\ActiveRecord
{
    const integer = 1;
    const string = 2;
    const text = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'field_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(Field::className(), ['type_id' => 'id']);
    }

    public  static  function getList()
    {
        $cache = new CacheComponent(['id' => 'FieldTypeGetList']);
        return $cache->get(function () {
           return ArrayHelper::map(FieldType::find()->asArray()->all(), 'id', 'name');
        });
    }
}
