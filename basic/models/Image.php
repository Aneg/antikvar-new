<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $updated
 * @property string $created
 * @property string $avatarImage
 *
 * @property Good[] $goods
 * @property GoodImage[] $goodImages
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['updated', 'created'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'updated' => 'Updated',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Good::className(), ['id' => 'good_id'])->viaTable('good_image', ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodImages()
    {
        return $this->hasMany(GoodImage::className(), ['image_id' => 'id']);
    }

    public function getAvatarImage() {
        $res = "<img src='{$this->image}' class='file-preview-image' alt='{$this->name}' title='{$this->name}'>";
        return $res;
    }

    public function afterDelete()
    {
        $absPass = Yii::getAlias("@webroot").$this->image;
        if (is_file($absPass)) unlink($absPass);

        return parent::afterDelete();
    }
}
