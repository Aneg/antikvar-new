<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "field".
 *
 * @property integer $id
 * @property string $name
 * @property integer $required
 * @property integer $dictionary
 * @property integer $preview
 * @property integer $type_id
 * @property string $about
 * @property string $updated
 * @property string $created
 * @property array $valueDict
 *
 * @property CategoryField[] $categoryFields
 * @property Category[] $categories
 * @property FieldType $type
 * @property Value[] $values
 */
class Field extends \yii\db\ActiveRecord
{

    public static function getDictionaryList()
    {
        return [
            true => "Да",
            false => "Нет",
        ];
    }

    public static function getRequiredList()
    {
        return [
            true => "Да",
            false => "Нет",
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id'], 'required'],
            [['required', 'dictionary', 'type_id', 'preview'], 'integer'],
            [['about'], 'string'],
            [['updated', 'created'], 'safe'],
            [['name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Hазвание',
            'required' => 'Обязательно',
            'dictionary' => 'Словарь',
            'preview' => 'Превью',
            'type_id' => 'Тип',
            'about' => 'Описание',
            'updated' => 'Обновлено',
            'created' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFields()
    {
        return $this->hasMany(CategoryField::className(), ['field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('category_field', ['field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(FieldType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(Value::className(), ['field_id' => 'id']);
    }

    public function getValueDict()
    {
        $result = [];
        foreach ($this->values as $value) {
            $result[$value->id] = $value->value;
        }
        return $result;
    }

    public function getFieldView($form, $params)
    {
        $result = '';

        if (!empty($params['good_id'])) {
            $good_id = $params['good_id'];
        }

        // Шаблон
        $template = !empty($params['placeholder']) ? ['template' => '{input}'] : ['template' => '{label}{input}'];

        // Значение
        if (!$this->dictionary) {
            if ((!empty($good_id) && !($value = Value::find()
                    ->joinWith('goodValues')
                    ->where(['value.field_id' => $this->id, 'good_value.good_id' => $good_id])
                    ->one()
                )) || empty($good_id)) {
                $value = new Value();
                $value->field_id = $this->id;
            }
            $result .= $form->field($value, '['.$params['key'].']field_id', ['template' => '{input}'])->hiddenInput();
        } else {
            if ((!empty($good_id) && !($value = GoodValue::find()
                    ->joinWith('value')
                    ->where(['value.field_id' => $this->id, 'good_id' => $good_id])
                    ->one())) ||
                empty($good_id)
            ) {
                $value = new GoodValue();
                if (!empty($good_id)) {
                    $value->good_id = $good_id;
                }
            }
            if (!empty($good_id)) {
                $result .= $form->field($value, '['.$params['key'].']good_id', ['template' => '{input}'])->hiddenInput();
            }
        }

        // TODO: сделать проверку на число
        if ($this->type_id == FieldType::integer) {
            if ($this->dictionary) {
                $result .= $form->field($value, '['.$params['key'].']value_id', $template)->dropDownList(
                    $this->required ? $this->valueDict :  ArrayHelper::merge([''], $this->valueDict),
                    $this->required ? ['prompt' => $this->name] : []
                );
            } else {
                $result .= $form->field($value, '['.$params['key'].']value', $template)->textInput([
                    'placeholder' => $this->name,
                ]);
                $result .= $form->field($value, '['.$params['key'].']id', ['template' => '{input}'])->hiddenInput();
            }
        } elseif ($this->type_id == FieldType::text) {
            $result .= $form->field($value, '['.$params['key'].']value', $template)->textarea([
                'rows' => 3,
                'placeholder' => $this->name,
            ]);
        } elseif ($this->type_id == FieldType::string) {
            if ($this->dictionary) {
                $result .= $form->field($value, '['.$params['key'].']value_id', $template)->dropDownList($this->valueDict, ['prompt' => $this->name]);
            } else {
                $result .= $form->field($value, '['.$params['key'].']value', $template)->textInput([
                    'placeholder' => $this->name,
                ]);
                $result .= $form->field($value, '['.$params['key'].']id', ['template' => '{input}'])->hiddenInput();
            }
        }
        return $result;
    }
}
