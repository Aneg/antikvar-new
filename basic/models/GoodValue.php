<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "good_value".
 *
 * @property integer $good_id
 * @property integer $value_id
 *
 * @property Value $value
 */
class GoodValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id', 'value_id'], 'required'],
            [['good_id', 'value_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'good_id' => 'Good ID',
            'value_id' => 'Value ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue()
    {
        return $this->hasOne(Value::className(), ['id' => 'value_id']);
    }
}
