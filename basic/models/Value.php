<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "value".
 *
 * @property integer $id
 * @property integer $field_id
 * @property string $value
 * @property string $updated
 * @property string $created

 *
 * @property GoodValue[] $goodValues
 * @property Field $field
 */
class Value extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_id'], 'required'],
            [['field_id'], 'integer'],
            [['updated', 'created'], 'safe'],
            [['value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'field_id' => 'Поле',
            'value' => 'Значение',
            'updated' => 'Обновлено',
            'created' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodValues()
    {
        return $this->hasMany(GoodValue::className(), ['value_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(Field::className(), ['id' => 'field_id']);
    }


}
