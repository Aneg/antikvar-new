<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * TODO: добавить кол-во просмотров.
 * TODO: выбор полей для превью
 * This is the model class for table "good".
 *
 * @property integer $id
 * @property integer $image_id
 * @property string $name
 * @property integer $category_id
 * @property string $about
 * @property string $updated
 * @property string $created
 * @property string $active
 * @property string $counter
 * @property array $items
 *
 * @property Category $category
 * @property Image $image
 * @property GoodImage[] $goodImages
 * @property Image[] $images
 * @property GoodLabel[] $goodLabels
 * @property Label[] $labels
 * @property Value[] $values
 */
class Good extends \yii\db\ActiveRecord
{
    private $items;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_id', 'category_id', 'counter', 'active'], 'integer'],
            [['name', 'category_id'], 'required'],
            [['about'], 'string'],
            [['updated', 'created'], 'safe'],
            [['name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Главное изображение',
            'counter' => 'Щётчик',
            'active' => 'Активность',
            'name' => 'Имя',
            'category_id' => 'Категория',
            'about' => 'Пояснение',
            'updated' => 'Обновлено',
            'created' => 'Созданно',
        ];
    }

    public function attributeLabel($name)
    {
//        return $this->getAttributeLabel()
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodImages()
    {
        return $this->hasMany(GoodImage::className(), ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['id' => 'image_id'])->viaTable('good_image', ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodLabels()
    {
        return $this->hasMany(GoodLabel::className(), ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabels()
    {
        return $this->hasMany(Label::className(), ['id' => 'label_id'])->viaTable('good_label', ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(Value::className(), ['id' => 'value_id'])->viaTable('good_value', ['good_id' => 'id']);
    }

    /**
     * Возвращает обьекты для клиентской части
     * @param array $where
     * @param array $with
     * @return array|\yii\db\ActiveRecord[]
     */
    static public function findAllByClient(array $where, array $with = [], $count = null)
    {
        $query =  self::find()
            ->where($where)
            ->joinWith(['category'])
            ->andWhere(['good.active' => '1', 'category.active' => 1]);

        foreach ($with as $relation) {
            $query->with($relation);
        }

        if ($count) $query->limit($count);
        return $query->all();
    }

    /**
     * Возвращает обьект для клиентской части
     * @param array $where
     * @param array $with
     * @return array|null|\yii\db\ActiveRecord
     */
    static public function findOneByClient(array $where, array $with = [])
    {
        $query =  self::find()
            ->where($where)
            ->joinWith(['category'])
            ->andWhere(['good.active' => '1', 'category.active' => 1]);

        foreach ($with as $relation) {
            $query->with($relation);
        }
        return $query->one();
    }
    
    public function getItems()
    {
        return $this->items;
    }
    
    /**
     * @return array
     */
    public function initFieldValue () {
        $this->items = $this->category->getItemsToUpdate(['good_id' => $this->id]);
        
        foreach ($this->items as $key1 => $value) {
            if (!Model::loadMultiple($this->items[$key1], Yii::$app->request->post(), $key1)) {
                return false;   
            }
        }
        return true;
    }
}
