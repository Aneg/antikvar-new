<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "good_label".
 *
 * @property integer $good_id
 * @property integer $label_id
 *
 * @property Good $good
 * @property Label $label
 */
class GoodLabel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good_label';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id', 'label_id'], 'required'],
            [['good_id', 'label_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'good_id' => 'Good ID',
            'label_id' => 'Label ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Good::className(), ['id' => 'good_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabel()
    {
        return $this->hasOne(Label::className(), ['id' => 'label_id']);
    }
}
