<?php

namespace app\models;

use app\components\CacheComponent;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "label".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $updated
 * @property string $created
 *
 * @property CategoryLabel[] $categoryLabels
 * @property Category[] $categories
 * @property GoodLabel[] $goodLabels
 * @property Good[] $goods
 * @property Category $category
 */
class Label extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'label';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['name'], 'required'],
            [['updated', 'created'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'name' => 'Имя',
            'updated' => 'Обновлено',
            'created' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryLabels()
    {
        return $this->hasMany(CategoryLabel::className(), ['label_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('category_label', ['label_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodLabels()
    {
        return $this->hasMany(GoodLabel::className(), ['label_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Good::className(), ['id' => 'good_id'])->viaTable('good_label', ['label_id' => 'id']);
    }

    public static function getLabelsWithoutThis($categoryId)
    {
        $cache = new CacheComponent(['id' => "labelaWithout-{$categoryId}"]);
        return $cache->get(function () use ($categoryId) {
            return ArrayHelper::map(self::find()
                ->joinWith('categories')
                ->where(['!=','category.id',$categoryId])
                ->asArray()->all(), 'id', 'name');
        });
    }

    /**
     * Возвращает метки нля категории
     * @param integer $category_id
     * @return array
     */
    public static function getLabelsArray($category_id)
    {
        $cache = new CacheComponent(['id' => "getLabelsArray"]);
        return $cache->get(function () use($category_id) {
            return ArrayHelper::map(self::find()->where(['category_id' => $category_id])
                ->asArray()->all(), 'id', 'name');
        });
    }
}
