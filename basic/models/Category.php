<?php

namespace app\models;

use app\components\CacheComponent;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property integer $active
 * @property string $updated
 * @property string $created
 * @property array $fieldList
 *
 * @property Category $parent
 * @property Category[] $categories
 * @property CategoryField[] $categoryFields
 * @property Field[] $fields
 * @property CategoryLabel[] $categoryLabels
 * @property Label[] $labels
 * @property Good[] $goods
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'active'], 'integer'],
            [['name'], 'required'],
            [['updated', 'created'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'active' => 'Показывать',
            'name' => 'Название',
            'updated' => 'Изменен',
            'created' => 'Создан',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFields()
    {
        return $this->hasMany(CategoryField::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(Field::className(), ['id' => 'field_id'])->viaTable('category_field', ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryLabels()
    {
        return $this->hasMany(CategoryLabel::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabels()
    {
        return $this->hasMany(Label::className(), ['id' => 'label_id'])->viaTable('category_label', ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Good::className(), ['category_id' => 'id']);
    }

    public static function getParentList($id = false)
    {
        $cache = new CacheComponent(['id' => 'CategoryGetParentList']);

        return $cache->get(function() use ($id) {
            $query = self::find()->andFilterWhere(['parent_id' => null]);
            if (!empty($id)) {
                $query->andFilterWhere(['!=', 'id', $id]);
            }
            return ArrayHelper::map($query->asArray()->all(),'id','name');
        });
    }

    public static function getList($param = ['view' => false])
    {
        $cache = new CacheComponent(['id' => 'CategoryGetList'.!empty($param['view'])]);

        return $cache->get(function() use ($param) {
            $query = Category::find()->select(['id', 'name', 'parent_id'])->asArray()->all();
            $result = [];
            if (!empty($param['view'])) {
                foreach ($query as $row) {
                    $result[$row['id']] = empty($row['parent_id']) ? $row['name'] : "- {$row['name']}";
                }
            } else {
                foreach ($query as $row) {
                    $result[$row['id']] = $row['name'];
                }
            }
            return $result;
        });
    }

    /**
     * @param $params array|boolean good_id
     * @return array обьекты полей категории
     */
    public function getItemsToUpdate($params = false) {

        if (!empty($params['good_id'])) {
            $good_id = $params['good_id'];
        }

        $result = [];

        foreach ($this->fields as $field) {
            if (!$field->dictionary) {
                if ((!empty($good_id) && !($value = Value::find()
                            ->joinWith('goodValues')
                            ->where(['value.field_id' => $field->id, 'good_value.good_id' => $good_id])
                            ->one()
                        )) || empty($good_id)) {
                    $value = new Value();
                    $value->field_id = $field->id;
                }
                $result['Value'][] = $value;
            } else {
                if ((!empty($good_id) && !($value = GoodValue::find()
                            ->joinWith('value')
                            ->where(['value.field_id' => $field->id, 'good_id' => $good_id])
                            ->one())) ||
                    empty($good_id)
                ) {
                    $value = new GoodValue();
                    if (!empty($good_id)) {
                        $value->good_id = $good_id;
                    }
                }
                $result['GoodValue'][] = $value;
            }
        }
        return $result;
    }
}
