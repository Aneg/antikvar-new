<?php

use yii\db\Schema;
use yii\db\Migration;
use \app\rbac\Permissions;
use app\models\User;

class m170414_090751_rbac_init extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $manageUsers = $auth->createPermission(Permissions::MANAGE_USERS);
        $manageUsers->description = 'Manage users';
        $auth->add($manageUsers);

        $editGood = $auth->createPermission(Permissions::EDIT_GOOD);
        $editGood->description = 'edit good';
        $auth->add($editGood);

        $editDictionary = $auth->createPermission(Permissions::EDIT_DICTIONATY);
        $editDictionary->description = 'Edit dictionary';
        $auth->add($editDictionary);

        $seeContact = $auth->createPermission(Permissions::SEE_CONTACT);
        $seeContact->description = 'See contact';
        $auth->add($seeContact);


        $user = $auth->createRole(User::ROLE_USER);
        $auth->add($user);
        $auth->addChild($user, $seeContact);

        $manager = $auth->createRole(User::ROLE_MANAGER);
        $auth->add($manager);
        $auth->addChild($manager, $editDictionary);
        $auth->addChild($manager, $editGood);
        $auth->addChild($manager, $user);

        $admin = $auth->createRole(User::ROLE_ADMIN);
        $auth->add($admin);
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $manager);
    }

    public function down()
    {

    }
}
