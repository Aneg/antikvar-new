<?php

use yii\db\Schema;
use yii\db\Migration;

class m160306_121449_alter_good extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Good::tableName(), 'active', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1');
        $this->addColumn(\app\models\Field::tableName(), 'preview', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0');
        $this->addColumn(\app\models\Good::tableName(), 'counter', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(\app\models\Good::tableName(), 'active');
        $this->dropColumn(\app\models\Field::tableName(), 'preview');
        $this->dropColumn(\app\models\Good::tableName(), 'counter');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
