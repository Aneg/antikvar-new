<?php

use yii\db\Schema;
use yii\db\Migration;

class m150620_190928_create_db extends Migration
{
    public function up()
    {
        $this->createTable('category', [
            'id' => Schema::TYPE_PK,
            'parent_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'updated' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_category_parent','category','parent_id','category','id');

        $this->createTable('image', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'image' => Schema::TYPE_STRING . '(150) NOT NULL',
            'updated' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createTable('good', [
            'id' => Schema::TYPE_PK,
            'image_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING . '(150) NOT NULL',
            'category_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'about' => Schema::TYPE_TEXT,
            'updated' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_good_image_id','good','image_id','image','id');
        $this->addForeignKey('fk_good_category_id','good','category_id','category','id');

        $this->createTable('good_image', [
            'good_id' => Schema::TYPE_INTEGER,
            'image_id' => Schema::TYPE_INTEGER,
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk_good_image','good_image',['good_id','image_id']);

        $this->addForeignKey('fk_good_image_good_id','good_image','good_id','good','id');
        $this->addForeignKey('fk_good_image_image_id','good_image','image_id','image','id');

        $this->createTable('field_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(150) NOT NULL',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->insert('field_type',[ 'id' => 1, 'name' => 'число']);
        $this->insert('field_type',[ 'id' => 2, 'name' => 'сторка']);
        $this->insert('field_type',[ 'id' => 3, 'name' => 'текст']);

        $this->createTable('field', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(150) NOT NULL',
            'required' => Schema::TYPE_BOOLEAN.' NOT NULL DEFAULT 0',
            'dictionary' => Schema::TYPE_BOOLEAN.' NOT NULL DEFAULT 0',
            'type_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'about' => Schema::TYPE_TEXT,
            'updated' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_field_type_id', 'field', 'type_id', 'field_type', 'id');

        $this->createTable('category_field', [
            'category_id' => Schema::TYPE_INTEGER,
            'field_id' => Schema::TYPE_INTEGER,
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk_category_field','category_field',['category_id','field_id']);
        $this->addForeignKey('fk_category_field_category_id','category_field','category_id','category','id');
        $this->addForeignKey('fk_category_field_field_id','category_field','field_id','field','id');

        $this->createTable('value', [
            'id' => Schema::TYPE_PK,
            'field_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'value' => Schema::TYPE_STRING . '(150) NOT NULL',
            'updated' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_value_field_id', 'value', 'field_id', 'field', 'id');

        $this->createTable('good_value', [
            'good_id' => Schema::TYPE_INTEGER,
            'value_id' => Schema::TYPE_INTEGER,
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk_good_value','good_value',['good_id','value_id']);
        $this->addForeignKey('fk_good_value_good_id','good_image','good_id','good','id');
        $this->addForeignKey('fk_good_value_image_id','good_value','value_id','value','id');

        $this->createTable('label', [
            'id' => Schema::TYPE_PK,
            'category_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'updated' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->addForeignKey('fk_label_category_id','label','category_id','category','id');

        $this->createTable('good_label', [
            'good_id' => Schema::TYPE_INTEGER,
            'label_id' => Schema::TYPE_INTEGER,
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk_good_label','good_label',['good_id','label_id']);
        $this->addForeignKey('fk_good_label_good_id', 'good_label', 'good_id', 'good', 'id');
        $this->addForeignKey('fk_good_label_label_id', 'good_label', 'label_id', 'label', 'id');

        $this->createTable('category_label', [
            'category_id' => Schema::TYPE_INTEGER,
            'label_id' => Schema::TYPE_INTEGER,
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk_category_label','category_label',['category_id','label_id']);
        $this->addForeignKey('fk_category_label_category_id', 'category_label', 'category_id', 'category', 'id');
        $this->addForeignKey('fk_category_label_label_id', 'category_label', 'label_id', 'label', 'id');
    }

    public function down()
    {
        echo "m150620_190928_create_db cannot be reverted.\n";

        return false;
    }
}
