<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\Category;

class m160312_234500_add_active_by_category extends Migration
{
    public function up()
    {
        $this->addColumn(Category::tableName(), 'active', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn(Category::tableName(), 'active');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
