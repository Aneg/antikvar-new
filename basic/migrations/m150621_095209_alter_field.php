<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_095209_alter_field extends Migration
{
    public function up()
    {
        $this->alterColumn(\app\models\Field::tableName(),'name', Schema::TYPE_STRING . '(150)');
    }

    public function down()
    {
    }
    

}
